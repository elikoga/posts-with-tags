<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    public function comments()
    {
        $this->hasMany('App\Models\Comment');
    }

    public function upvoters()
    {
        $this->belongsToMany('App\Models\User')->as('upvotes');
    }
}
