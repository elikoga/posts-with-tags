<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

use App\Models\User;


/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('createAdminUser', function () {
    $adminUser = User::where('name', 'admin')->first();
    if(!$adminUser) {
        $adminUser = new User;
    }
    $adminUser->name = 'admin';
    $adminUser->email = 'admin@example.com';
    $adminUser->password = Hash::make('password');
    $adminUser->save();
});