<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->timestampsTz();
            
            $table->enum('commentable_type', ['post', 'comment']);
            $table->foreignId('post_id')->nullable();
            $table->foreign('post_id')->references('id')->on('posts');
            $table->foreignId('comment_id')->nullable();
            $table->foreign('comment_id')->references('id')->on('comments');

            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('content');
            $table->bigInteger('score');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
