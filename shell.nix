{ nixpkgs ? import <nixpkgs> {} }:

let
  inherit (nixpkgs) pkgs;

  nixPackages = with nixpkgs; [
    nodejs
  ];
in
pkgs.stdenv.mkDerivation {
  name = "posts-with-tags-env";
  buildInputs = nixPackages;
}
